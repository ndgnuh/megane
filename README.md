# FCOS

This branch experiment with the FCOS detector model.

Project structures:
```bash
.
├── megane # Source folder
│   ├── coders.py # Target encoder/decoder, image processor
│   ├── datasets.py # Data, dataloaders, datamodules
│   └── metrics.py # Performance metric (MaP)
└── requirements.txt # Requirements
```

## How to...

#### Prepare a custom dataset for training

First install `dsrecords`.
```shell
pip install dsrecords
```

Write a function to iterate through your data:
```python
def data_iter(*args, **kwargs):
    data = load_my_data(*args, **kwargs)
    for image, boxes, classes in data:
        boxes = 
        yield image, boxes, classes
```

Create a dense record dataset:
```python
from dsrecords import IndexedRecordDataset, io

# Replace save_pil with save_np, save_cv2, save_raw_file if your image
# is not a PIL.Image.Image
dumpers = [io.save_pil, io.save_np, io.save_np]
dataset = IndexedRecordDataset("custom-data.rec", serializers=dumpers)
for sample in data_iter:
    dataset.append(sample)
```

Two files will be created as follow, these are the dataset files:
```shell
custom-data.rec
custom-data.idx
```

#### Train a custom model

Generate a configuration file.
```sh
python -m megane mkconfig -o config.json
```

Customize the configuration:
```json
{
    "num_classes": 1,
    "image_width": 300,
    "image_height": 300,
    "detection_threshold": 0.6,
    "nms_threshold": 0.4,
    "load_weights": "pretrained-weight-if-any.pt",
    "save_weights": "fcos-custom.pt",
    "onnx_path": "fcos-custom.onnx",
    "train_data": ["custom-data.rec"],
    "test_data": [],
    "val_data": [],
    "augment_prob": 0.333333333333,
    "augment_features": [
        "color_transform",
        "weather_transform"
    ]
}
```

Train the model:
```sh
# Run python -m megane train --help for all the options
python -m megane train -c config.json --shuffle -b 16 -p 12
```

#### Using ONNX Weights

While training, ONNX model will be exported to `onnx_path`. This onnx file inputs only one image end returns the prediction for that single image.
```python
import cv2
from onnxruntime import InferenceSession

from megane.configs import read_config
from megane.coders import ImageProcessor

image = cv2.imread("my-image.png")
config = read_config("my-config.json")
preprocess = ImageProcessor(width=config['image_width'], height=config['image_height'])
model = InferenceSession("./fcos-custom.onnx")
boxes, classes, scores = model.run(None, {"image": preprocess(image)})
```

If you don't want to install `menage`, that's fine, this `preprocess` function will do just fine:
```python
def preprocess(image, width: int, height: int):
    image = cv2.resize(image, (width, height))
    image = image.astype('float32') / 255
    image = image.transpose(2, 0, 1)
    return image

import cv2
import json
from onnxruntime import InferenceSession

with open("my-config.json") as f:
    config = json.load(f)

image = cv2.imread("my-image.png")
image_input = preprocess(image, width=config["image_width"], height=config["image_height"])
model = InferenceSession("./fcos-custom.onnx")
boxes, classes, scores = model.run(None, {"image": image_input})
```

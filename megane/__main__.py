import json
from argparse import ArgumentParser
try:
    import icecream
    icecream.install()
except ImportError:
    pass

from .configs import example_config, read_config, validate_config

prog = "megane"
actions = {}


def add_action(f):
    this = f
    actions[f.__name__] = f
    return f


@add_action
def prototype(argv):
    import torch

    from megane.coders import encode_fcos_ms
    from megane.datasets import DetectionDataset

    data = DetectionDataset("/home/hung/Data/faces_30-10-2023.rec")
    image, (boxes, classes) = data[0]
    H, W, C = image.shape
    ms_targets = encode_fcos_ms(
        boxes, classes, num_classes=1, image_width=1024, image_height=1024
    )
    for target in ms_targets:
        print([x.shape for x in target])


@add_action
def search_num_hack(argv):
    parser = ArgumentParser()
    parser.add_argument("--config", "-c", required=True)
    args = parser.parse_args(argv)

    # +------------------+
    # | Check the config |
    # +------------------+
    config = read_config(args.config)
    validate_config(config, train=True)

    import numpy as np

    from .coders import ImageProcessor
    from .datasets import DetectionDataModule

    data = DetectionDataModule(
        train_data=config["train_data"],
        test_data=config.get("test_data", []),
        val_data=config.get("val_data", []),
    )

    W, H = config["image_width"], config["image_height"]
    scaler = np.array([W, H, W, H]).reshape(1, 4)
    num_hack = 1
    for image, boxes, classes in data.train_dataloader():
        boxes = (boxes * scaler / 4).T
        widths = (boxes[2] - boxes[0]).mean()
        heights = (boxes[3] - boxes[1]).mean()
        num_hack = max(num_hack, widths, heights)
        print("Numerical hack value: ", num_hack)


@add_action
def train(argv):
    parser = ArgumentParser(prog="{prog} train")
    parser.add_argument("--config", "-c", required=True)
    parser.add_argument("--batch-size", "-b", default=1, type=int)
    parser.add_argument("--shuffle", default=False, action="store_true")
    parser.add_argument("--lr", type=float, default=1e-2)
    parser.add_argument("--num-workers", "-p", default=0, type=int)
    parser.add_argument("--clip", default=1000, type=float)

    args = parser.parse_args(argv)

    # +------------------+
    # | Check the config |
    # +------------------+
    config = read_config(args.config)
    validate_config(config, train=True)

    # +-----------------------------------+
    # | Initialize training and ... train |
    # +-----------------------------------+
    from lightning import Trainer

    from .training import Learner

    learner = Learner(config, args)
    trainer = Trainer(log_every_n_steps=5)
    trainer.fit(learner)


@add_action
def mkconfig(argv):
    parser = ArgumentParser(prog=f"{prog} mkconfig")
    parser.add_argument("-o", "--output", help="Output config file")
    parser.add_argument("--name", help="Model name, anything", default="fcos")
    args = parser.parse_args(argv)

    config = example_config(name=args.name)
    config_json = json.dumps(config, ensure_ascii=False, indent=4)
    if args.output is None:
        print(config_json)
    else:
        with open(args.output, "w") as f:
            f.write(config_json)


def main():
    parser = ArgumentParser(prog=prog)
    parser.add_argument("action", choices=list(actions))
    args, unknown_args = parser.parse_known_args()

    dispatch = actions[args.action]
    dispatch(unknown_args)


if __name__ == "__main__":
    main()

# A helper class to help slicing through the multi scale mess

from dataclasses import dataclass

none_slice = slice(None, None, None)


def get_slice(x, *idx):
    if len(idx) == 0:
        return x

    i, idx = idx[0], idx[1:]

    # Special case: reduce dimension
    if isinstance(i, int):
        return get_slice(x[i], *idx)

    if isinstance(i, (list, tuple)):
        x = [x[i] for i in i]
    else:
        x = x[i]

    return [get_slice(x_i, *idx) for x_i in x]


@dataclass
class Slicer:
    x: object

    def __getitem__(self, idx):
        x = get_slice(self.x, *idx)
        return Slicer(x)

    def guess_shape(self, max_iter=1000):
        lens = []
        x = self.x
        for i in range(max_iter):
            try:
                n = len(x[0])
                x = x[0]
                if n == 0:
                    break
                lens.append(n)
            except Exception:
                break
        return lens

    def get(self):
        return self.x

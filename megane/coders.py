from collections import defaultdict
from dataclasses import dataclass
from functools import cached_property, partial
from typing import Callable, List, Tuple, Union

import cv2
import numpy as np
from simpoly.bbox_utils import nms as py_nms

DEFAULT_NUM_HACK = 10


def ImageProcessor(width: int, height: int):
    return partial(prepare_input, width=width, height=height)


@dataclass
class TargetCoder:
    num_classes: int
    image_width: int
    image_height: int
    strides: Union[int, List[int]] = 4
    detection_threshold: float = 0.6
    nms_threshold: float = 0.6

    @cached_property
    def encode_options(self):
        options = {
            "num_classes": self.num_classes,
            "image_width": self.image_width,
            "image_height": self.image_height,
            "num_hack": DEFAULT_NUM_HACK,
        }
        if isinstance(self.strides, int):
            options["stride"] = self.strides
        else:
            options["strides"] = self.strides
        return options

    @cached_property
    def decode_options(self):
        return {
            "detection_threshold": self.detection_threshold,
            "nms_threshold": self.nms_threshold,
            "num_hack": DEFAULT_NUM_HACK,
        }

    @cached_property
    def is_multi_scales(self):
        return isinstance(self.strides, list)

    @cached_property
    def decode_fn(self):
        return decode_fcos_ms if self.is_multi_scales else decode_fcos_ss

    @cached_property
    def encode_fn(self):
        return encode_fcos_ms if self.is_multi_scales else encode_fcos_ss

    def encode(self, targets):
        return self.encode_fn(targets, **self.encode_options)

    def decode(self, outputs):
        return self.decode_fn(outputs, **self.decode_options)

    def decode_torch(self, outputs):
        import torch
        from torchvision.ops import nms

        extra_options = dict(nms_fn=nms, stack_fn=torch.stack, where_fn=torch.where)
        return self.decode_fn(outputs, **self.decode_options, **extra_options)


def create_region(box, width: int, height: int):
    """Returns the drawing region of the bounding box inside the canvas.

    Args:
        box (ndarray): the n-xyxy unnormalized bounding box
        width (int): canvas width
        height (int): canvas height

    Returns:
        box (tuple[int, int, int, int]): clipped x1 y1 x2 y2 coordinates.
    """

    x1, y1, x2, y2 = np.round(box).astype(int)
    x1, x2 = max(x1, 0), min(x2, width)
    y1, y2 = max(y1, 0), min(y2, height)
    return x1, y1, x2, y2


def draw_centerness(canvas: np.ndarray, box, region=None):
    """Draw centerness of box and paste to a region inside the canvas

    Args:
        canvas (np.ndarray): canvas to draw on
        box (np.ndarray): unnormalized canvas size xyxy bounding box,
            can exceed the canvas
        region (None | np.ndarray): region hint, if none it will be calculated
    """
    # make drawing grid
    H, W = canvas.shape
    if region is None:
        region = create_region(box, W, H)
    x1, y1, x2, y2 = region
    xs = np.arange(x1, x2)[None, :]
    ys = np.arange(y1, y2)[:, None]
    if xs.size == 0 or ys.size == 0:
        return

    # add the center fraction so that the center value is truely 1
    x1, y1, x2, y2 = box
    cx = (x1 + x2) / 2
    cy = (y1 + y2) / 2
    ys = ys + cy - int(cy)
    xs = xs + cx - int(cx)

    # distance to border
    l, r = xs - x1, x2 - xs
    t, b = ys - y1, y2 - ys

    # centerness
    lr = np.fmin(l, r) / np.fmax(l, r)
    tb = np.fmin(t, b) / np.fmax(t, b)
    ctn = np.sqrt(np.clip(lr * tb, 0, np.inf))
    ctn_max = ctn.max()
    assert ctn_max >= 0.999999, f"Centerness maximum value is {ctn_max} but not 1"

    # Paste on the canvas
    x1, y1, x2, y2 = region
    canvas[y1:y2, x1:x2] = np.fmax(canvas[y1:y2, x1:x2], ctn)


def draw_offsets(canvas: np.ndarray, box, region=None):
    """Draw L/T/R/B offset map on the canvas.

    Args:
        canvas (np.ndarray): canvas of shape [4, H, W] to be drawn on.
        box (np.ndarray): unnormalized canvas size xyxy bounding box,
            can exceed the canvas
        region (Optional[np.ndarray]): region hint, will be calculated if none
    """
    _, H, W = canvas.shape
    if region is None:
        region = create_region(box, W, H)
    x1, y1, x2, y2 = region

    # Drawing grid
    xs = np.arange(x1, x2)[None, :]
    ys = np.arange(y1, y2)[:, None]

    # Offset values
    x1, y1, x2, y2 = box
    l, r = (xs - x1) / W, (x2 - xs) / W
    t, b = (ys - y1) / H, (y2 - ys) / H
    ltrb = np.stack(np.broadcast_arrays(l, t, r, b), 0)

    # Paste on canvas
    x1, y1, x2, y2 = region
    canvas[:, y1:y2, x1:x2] = np.fmax(canvas[:, y1:y2, x1:x2], ltrb)


def encode_fcos_ms(
    targets: Tuple[np.ndarray, np.ndarray],
    num_classes: int,
    image_width: int,
    image_height: int,
    strides: List[int] = [4, 8, 16, 32, 64, 128],
    num_hack: float = DEFAULT_NUM_HACK,
):
    boxes, classes = targets
    W, H, C = image_width, image_height, num_classes
    classes = np.array(classes)
    boxes = np.array(boxes)

    # Sort boxes by area descending
    if len(boxes) > 0:
        x1, y1, x2, y2 = boxes.transpose(1, 0)
        area = (x2 - x1) * (y2 - y1)
        idx = np.argsort(-area)
        boxes = boxes[idx]
        classes = classes[idx]

    # Find strides
    box_by_strides = defaultdict(list)
    finder = StrideFinder(strides)
    scale = np.array([W, H, W, H])
    for i, norm_box in enumerate(boxes):
        _, stride = finder.find_stride(norm_box * scale)
        box_by_strides[stride].append(i)

    # Encode for each strides
    ms_targets = []
    for stride in strides:
        idx = np.array(box_by_strides[stride])
        target = (
            boxes[idx] if len(idx) > 0 else [],
            classes[idx] if len(idx) > 0 else [],
        )
        target = encode_fcos_ss(
            target,
            num_classes=num_classes,
            image_width=image_width,
            image_height=image_height,
            stride=stride,
            num_hack=num_hack,
        )
        ms_targets.append(target)
    return ms_targets


def encode_fcos_ss(
    targets: Tuple[np.ndarray, np.ndarray],
    num_classes: int,
    image_width: int,
    image_height: int,
    stride: int,
    num_hack: float = DEFAULT_NUM_HACK,
):
    """Encode FCOS target in one single scale.

    Args:
        targets (Tuple[np.ndarray, np.ndarray]):
            Tuple of normalized xyxy bounding boxes and class indices.
        num_classes (int): total number of classes C
        image_width (int): image width
        image_height (int): image height
        stride (int): target stride
        num_hack (int): multipiler to help convergence for offsets, default 10

    Returns:
        objectness (np.ndarray): centerness and regression map of shape [C, H, W]
        centerness (np.ndarray): centerness and regression map of shape [1, H, W]
        offsets (np.ndarray): centerness and regression map of shape [4, H, W]
    """
    # Metrics
    C = num_classes
    W, H = get_target_size(image_width, image_height, stride)
    boxes, classes = targets

    # Targets
    objectness = np.zeros([C, H, W], dtype=np.float32)
    centerness = np.zeros([1, H, W], dtype=np.float32)
    offsets = np.zeros([4, H, W], dtype=np.float32)
    scale = np.array([W, H, W, H])

    # Draw targets
    for c, norm_box in zip(classes, boxes):
        box = norm_box * scale
        region = create_region(box, W, H)
        x1, y1, x2, y2 = region
        objectness[c, y1:y2, x1:x2] = 1
        draw_centerness(centerness[0], box, region)
        draw_offsets(offsets, box, region)
    offsets = offsets * num_hack
    return objectness, centerness, offsets


def decode_fcos_ss(
    targets: Tuple[np.ndarray, np.ndarray, np.ndarray],
    detection_threshold: float = 0.6,
    nms_threshold: float = 0.4,
    num_hack: float = DEFAULT_NUM_HACK,
    where_fn: Callable = np.where,
    stack_fn: Callable = np.stack,
    nms_fn: Callable = lambda b, s, t: py_nms(b.tolist(), s.tolist(), t),
):
    """Decode FCOS detection result from target maps.

    Args:
        targets (Tuple): Tuple of encoded targets
        min_score (float): minimum detection score (default 0.6)
        iou_threshold (float): iou threshold for nms (default 0.4)
        num_hack (float): numerical hack for convergence (default 10)

    Targets:
        objectness (ndarray): numpy classification array of shape (C, H, W)
        centerness (ndarray): numpy centerness array of shape (1, H, W)
        offsets (ndarray): numpy offsets array of shape (4, H, W)

    Returns:
        boxes (np.ndarray): normalized xyxy bounding boxes
        classes (np.ndarray): class indices
        scores (np.ndarray): bounding box scores
    """
    # Unpack target
    objectness, centerness, offsets = targets
    _, H, W = offsets.shape
    score_maps = objectness * centerness

    # Get bboxes from target maps
    classes, ys, xs = where_fn(score_maps >= detection_threshold)
    scores = score_maps[classes, ys, xs]
    l, t, r, b = offsets[:, ys, xs] / num_hack
    cys, cxs = ys / H, xs / W
    boxes = stack_fn([cxs - l, cys - t, cxs + r, cys + b], 1)

    # NMS
    keep = nms_fn(boxes, scores, nms_threshold)
    boxes = boxes[keep]
    classes = classes[keep]
    scores = scores[keep]
    return boxes, classes, scores


def decode_fcos_ms(
    outputs: Tuple[np.ndarray, np.ndarray, np.ndarray],
    **kwargs,
):
    """
    Args:
        targets (List[Tuple]): List of tuple of encoded targets, each item is a scale
        kwargs (Dict): Any options for `decode_fcos_ss`.

    Targets:
        objectness (ndarray): numpy classification array of shape (C, H, W)
        centerness (ndarray): numpy centerness array of shape (1, H, W)
        offsets (ndarray): numpy offsets array of shape (4, H, W)
    """
    all_boxes = []
    all_classes = []
    all_scores = []
    for outputs_ss in outputs:
        boxes, classes, scores = decode_fcos_ss(outputs_ss, **kwargs)
        all_boxes.extend(boxes)
        all_classes.extend(classes)
        all_scores.extend(scores)
    return all_boxes, all_classes, all_scores


def prepare_input(image, width: int, height: int):
    """Prepare input image

    Args:
        image (np.ndarray): uint8 input image of shape [H, W, C]
        width (int): target width
        height (int): target height

    Returns:
        image (np.ndarray): resized float32 image of shape [C, newH, newW]
    """
    image = cv2.resize(image, (width, height))
    image = image.astype(np.float32) / 255
    image = image.transpose(2, 0, 1)
    return image


class StrideFinder:
    def __init__(self, strides):
        scale = min(strides) // 2
        bounds = [s * scale for s in strides]
        self.strides = strides
        self.lower_bounds = [0] + bounds[1:]
        self.upper_bounds = bounds[1:] + [float("inf")]

    def find_stride(self, box):
        n = len(self.strides)

        # Compute regression size
        x1, y1, x2, y2 = box
        cx = (x1 + x2) / 2
        cy = (y1 + y2) / 2
        max_regress = max(cx - x1, x2 - cx, y2 - cy, cy - y1)

        # Search from the largest stride (smallest ft map)
        for i in range(n - 1, -1, -1):
            # Lower bound by strides
            stride = self.strides[i]
            lb = self.lower_bounds[i]
            ub = self.upper_bounds[i]

            # Select scale
            if max_regress < lb or max_regress > ub:
                continue
            else:
                return i, stride
        return None


def get_target_size(W: int, H: int, S: int):
    """Return target size given the image size and stride.

    Simply calling W//S won't be correct if the image size is not divisable by 32

    Args:
        W (int): image width
        H (int): image height
        S (int): target stride
    """
    while S > 1:
        W = int(W / 2)
        H = int(H / 2)
        S = int(S / 2)
    return W, H

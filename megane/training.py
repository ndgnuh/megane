import warnings
from os import path
from typing import Dict

import torch
from lightning import LightningModule
from PIL import Image, ImageDraw
from torch import nn, optim
from torchvision.transforms.functional import resize, to_pil_image, to_tensor
from torchvision.utils import make_grid
from tqdm import tqdm

from .augmentations import get_augmentation
from .coders import ImageProcessor, TargetCoder
from .datasets import DetectionDataModule
from .nn import FCOS, FCOSCriterion, SingleScaleFCOS
from .slice_tools import Slicer


def draw_boxes(image: Image.Image, boxes, classes):
    image = image.copy()
    W, H = image.size
    draw = ImageDraw.Draw(image)
    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    for (x1, y1, x2, y2), c in zip(boxes, classes):
        box = [(x1 * W, y1 * H), (x2 * W, y2 * H)]
        draw.rectangle(box, outline=colors[c], width=2)
    return image


def mk_data_module(config: Dict, options, coder: TargetCoder):
    """Returns DataModule for training

    Args:
        config (Dict): Model config
        options (Namespace): Training options, obtained from argparse
        coder (TargetCoder): target encoder
    """
    return DetectionDataModule(
        train_data=config["train_data"],
        test_data=config.get("test_data", []),
        val_data=config.get("val_data", []),
        augment_fn=get_augmentation(config["augment_prob"], config["augment_features"]),
        encode_fn=coder.encode,
        process_input=ImageProcessor(
            width=config["image_width"],
            height=config["image_height"],
        ),
        dataloader_options=dict(
            batch_size=options.batch_size,
            num_workers=options.num_workers,
            shuffle=options.shuffle,
        ),
    )


class TrainingVisualizer:
    def __init__(self, coder: TargetCoder, batch_size: int, multiscale):
        self.coder = coder
        self.batch_size = batch_size
        self.multiscale = multiscale
        self.draw_boxes = self.draw_boxes_ms if multiscale else self.draw_boxes_ss

    @torch.no_grad()
    def draw_pred_maps(self, batch_outputs, post_process: bool):
        if self.multiscale:
            images = [self.draw_pred_maps_ss(x, post_process) for x in batch_outputs]
            H, W = max(image.shape for image in images)[-2:]
            images = [resize(image, (H, W)) for image in images]
            images = torch.cat(images, dim=-2)
            return images
        else:
            return self.draw_pred_maps_ss(batch_outputs, post_process)

    @torch.no_grad()
    def draw_pred_maps_ss(self, batch_outputs, post_process: bool):
        batch_objectness, batch_centerness, batch_offsets = batch_outputs
        if post_process:
            batch_objectness = batch_objectness.sigmoid()
            batch_centerness = batch_centerness.sigmoid()

        # Merge channels to columns
        images = [
            torch.cat(batch_objectness.unbind(1), -1),
            torch.cat(batch_centerness.unbind(1), -1),
            torch.cat(batch_offsets.unbind(1), -1),
        ]
        images = torch.cat(images, dim=-1)

        # Merge batches to rows
        images = torch.cat(images.unbind(0), dim=-2)
        return images.unsqueeze(0)

    @torch.no_grad()
    def draw_boxes_ms(self, images, ms_batch_outputs, post_process: bool):
        # Post-process
        # Shape: [Scale, targets, batch, etc.]
        slicer = Slicer(ms_batch_outputs)
        if post_process:
            objectness = [x.sigmoid() for x in slicer[:, 0].get()]
            centerness = [x.sigmoid() for x in slicer[:, 1].get()]
        else:
            objectness = [x for x in slicer[:, 0].get()]
            centerness = [x for x in slicer[:, 1].get()]
        offsets = [x for x in slicer[:, 2].get()]

        # Convert to list of outputs for coders
        # Shape: [targets, scales, batch] -> [batch, scales, targets]
        num_scales = len(ms_batch_outputs)
        batch_size = self.batch_size
        slicer = Slicer([objectness, centerness, offsets])
        outputs = [
            [
                (
                    slicer[0, s, b].get().cpu(),
                    slicer[1, s, b].get().cpu(),
                    slicer[2, s, b].get().cpu(),
                )
                for s in range(num_scales)
            ]
            for b in range(batch_size)
        ]

        # Common operations, let coders handle the decode
        return self.draw_boxes_common(images, outputs)

    @torch.no_grad()
    def draw_boxes_ss(self, images, batch_outputs, post_process):
        # Post process
        # Dims: [targets, batch]
        batch_size = self.batch_size
        if post_process:
            objectness = batch_outputs[0].sigmoid()
            centerness = batch_outputs[1].sigmoid()
        else:
            objectness = batch_outputs[0]
            centerness = batch_outputs[1]
        offsets = batch_outputs[2]

        # Convert to coder outputs
        # Dims: [targets, batch]
        slicer = Slicer([objectness, centerness, offsets])
        outputs = [slicer[:, b].get().cpu() for b in range(batch_size)]
        return self.draw_boxes_common(images, outputs)

    @torch.no_grad()
    def draw_boxes_common(self, images, batch_outputs):
        coder = self.coder
        batch_size = len(batch_outputs)
        outputs = []
        for image, output in zip(images, batch_outputs):
            image = to_pil_image(image)
            boxes, classes, _ = coder.decode_torch(output)
            image = draw_boxes(image, boxes, classes)
            image = to_tensor(image)
            outputs.append(image)

        # Returns grid of images
        outputs = torch.stack(outputs, dim=0)
        return make_grid(outputs, 2)


class Learner(LightningModule):
    def __init__(self, config: Dict, options):
        super().__init__()
        coder = TargetCoder(
            num_classes=config["num_classes"],
            image_width=config["image_width"],
            image_height=config["image_height"],
            strides=config["strides"],
            detection_threshold=config["detection_threshold"],
            nms_threshold=config["nms_threshold"],
        )
        multiscale = isinstance(config["strides"], (tuple, list))

        # Register layers, utilities, etc.
        self.save_hyperparameters()
        if multiscale:
            self.model = FCOS(config["num_classes"], config["strides"])
        else:
            self.model = SingleScaleFCOS(config["num_classes"])
        self.data = mk_data_module(config, options, coder)
        self.criterion = FCOSCriterion(multiscale)
        self.visualizer = TrainingVisualizer(
            coder=coder,
            batch_size=options.batch_size,
            multiscale=multiscale,
        )
        self.exporter = ModelWrapper(self.model, coder, multiscale)
        self.config = config
        self.options = options

        # Load model weights
        if path.isfile(config.get("load_weights", "/non-exist")):
            weights = torch.load(config["load_weights"], map_location="cpu")
            warnings = self.model.load_state_dict(weights, strict=False)
            print(warnings)
        else:
            tqdm.write(
                "Can't load weights because it is not specified or does not exists."
            )

    # +-------------------------+
    # | Lightning API functions |
    # +-------------------------+
    def training_step(self, batch, batch_idx):
        images, targets = batch
        outputs = self.model(images)
        losses = self.criterion(outputs, targets)

        # Log total loss
        loss = losses.pop("total")
        self.log("loss/total", loss.item(), prog_bar=True)

        # Log other losses
        for tag, value in losses.items():
            self.log(f"loss/{tag}", value.item())

        # Training callback
        if batch_idx % 250 == 0:
            # Visualize
            vis = self.visualizer
            pr_images = vis.draw_boxes(images, outputs, True)
            gt_images = vis.draw_boxes(images, targets, False)
            pr_maps = vis.draw_pred_maps(outputs, True)
            gt_maps = vis.draw_pred_maps(targets, False)
            self.add_image("train/10-pr-sample", pr_images)
            self.add_image("train/11-pr-maps", pr_maps)
            self.add_image("train/20-gt-sample", gt_images)
            self.add_image("train/21-gt-maps", gt_maps)

        return {"loss": loss, "outputs": outputs, "targets": targets}

    def train_dataloader(self):
        return self.data.train_dataloader()

    def val_dataloader(self):
        return self.data.val_dataloader()

    def configure_optimizers(self):
        lr = self.options.lr
        optimizer = optim.SGD(
            self.parameters(),
            lr=lr,
            weight_decay=1e-5,
            momentum=0.09,
        )
        return optimizer

    def on_train_epoch_end(self):
        self.save_weights()

    def on_train_batch_end(self, outputs, batch, batch_idx):
        # Save weights
        if batch_idx % 250 == 0 and self.global_step > 0:
            self.save_weights()

    # +------------------+
    # | Custom functions |
    # +------------------+
    def save_weights(self):
        # Unpack config
        model = self.model
        config = self.config
        weight_path = config["save_weights"]
        onnx_path = config["onnx_path"]

        # Export ONNX
        device = next(model.parameters()).device
        sample = torch.rand(3, config["image_height"], config["image_width"])
        model.set_post_processing(True)
        torch.onnx.export(
            self.exporter.eval(),
            sample.to(device),
            onnx_path,
            do_constant_folding=True,
            input_names=["image"],
        )
        model.set_post_processing(False)

        # Save weights
        weights = self.model.state_dict()
        torch.save(weights, weight_path)

        # Clean up and log
        self.model.train()
        tqdm.write(f"Model weight saved to {weight_path}, onnx exported to {onnx_path}")

    def add_image(self, tag, images):
        step = self.global_step
        step = 0  # Just overwrite the image, otherwise the log dir will be massive
        self.logger.experiment.add_image(tag, images, step)


class ModelWrapper(nn.Module):
    def __init__(self, model, coder, multiscale: bool, decoding: bool = True):
        super().__init__()
        self.model = model
        self.coder = coder
        self.multiscale = multiscale
        if decoding and hasattr(self.coder, "decode_torch"):
            self.decoding = True
        elif decoding:
            warnings.warn(
                f"This coder {coder} does not support decoding with pytorch, implement decode_torch decoding to work"
            )
            self.decoding = False
        else:
            self.decoding = False

    def forward(self, image):
        image = image[None]
        # Output shape:
        # multi scale: [Target, Batch, C, H, W]
        # single scale: [Scales, Target, Batch, C, H, W]
        outputs = self.model(image)
        outputs = Slicer(outputs)

        # Get the first sample of batch outputs
        if self.multiscale:
            outputs = outputs[:, :, 0].get()
        else:
            outputs = [outputs[:, 0].get()]

        # Decode for multiple scales
        if self.decoding:
            return self.coder.decode_torch(outputs)
        else:
            return outputs

from collections import defaultdict

import torch
from torch import nn
from torch.nn import functional as F
from torchvision.ops import sigmoid_focal_loss


class ExSigmoid(nn.Module):
    """In the master module, set `post_processing` to true and the heat map is all good"""

    def __init__(self):
        super().__init__()
        self.post_processing = False

    def forward(self, x):
        if self.post_processing:
            return x.sigmoid()
        else:
            return x


class BoxOutputFn(nn.Module):
    def __init__(self, scale: float = 0.1):
        super().__init__()
        # Since object tends to have landscape aspect ratio...
        scales = [scale * 1.5, scale, scale * 1.5, scale]
        self.scale = nn.Parameter(torch.tensor(scales).reshape(1, 4, 1, 1))

    def forward(self, x):
        return torch.exp(self.scale * x)


class FCOSHead(nn.Module):
    def __init__(self, input_size: int, num_classes: int):
        super().__init__()
        self.objectness = self._make_branch(input_size, num_classes, ExSigmoid())
        self.centerness = self._make_branch(input_size, 1, ExSigmoid())
        self.offsets = self._make_branch(input_size, 4, BoxOutputFn())

    def forward(self, x):
        return (self.objectness(x), self.centerness(x), self.offsets(x))

    def _make_branch(self, C1, C2, *extra_layers):
        return nn.Sequential(
            nn.Conv2d(C1, C1, 3, padding=1, bias=False),
            nn.BatchNorm2d(C1),
            nn.ReLU(True),
            nn.Conv2d(C1, C1, 3, padding=1, bias=False),
            nn.BatchNorm2d(C1),
            nn.ReLU(True),
            nn.Conv2d(C1, C1, 3, padding=1, bias=False),
            nn.BatchNorm2d(C1),
            nn.ReLU(True),
            nn.Conv2d(C1, C2, 3, padding=1),
            *extra_layers
        )


def _box_area(ltrb):
    l, t, r, b = ltrb.unbind(-1)
    w, h = [l + r, t + b]
    return w * h


def _inter_area(pr_ltrb, gt_ltrb):
    inter_l = torch.min(pr_ltrb[:, 0], gt_ltrb[:, 0])
    inter_t = torch.min(pr_ltrb[:, 1], gt_ltrb[:, 1])
    inter_r = torch.min(pr_ltrb[:, 2], gt_ltrb[:, 2])
    inter_b = torch.min(pr_ltrb[:, 3], gt_ltrb[:, 3])
    return (inter_l + inter_r) * (inter_t + inter_b)


def iou_loss(pr, gt, mask):
    # Target positions
    b, ys, xs = torch.where(mask)
    pr_ltrb = pr[b, :, ys, xs]
    gt_ltrb = gt[b, :, ys, xs]

    # Areas
    pr_area = _box_area(pr_ltrb)
    gt_area = _box_area(gt_ltrb)
    inter_area = _inter_area(pr_ltrb, gt_ltrb)
    union_area = pr_area + gt_area - inter_area

    # IOU
    iou = inter_area / union_area
    loss = 1 - iou
    return loss


class FCOSCriterion(nn.Module):
    def __init__(self, multi_scale: bool):
        super().__init__()
        self.multi_scale = multi_scale

    def forward(self, *args, **kwargs):
        if self.multi_scale:
            return self.forward_ms(*args, **kwargs)
        else:
            return self.forward_ss(*args, **kwargs)

    def forward_ms(self, ms_pr, ms_gt):
        # Group by scale
        keyfn = lambda x: x[0].shape[-2:]
        pr_by_scales = groupby(ms_pr, keyfn)
        gt_by_scales = groupby(ms_gt, keyfn)
        assert set(pr_by_scales) == set(gt_by_scales), f"Scales does not match: pr {set(pr_by_scales)} and gt: {set(gt_by_scales)}"

        # Compute multiscale losses
        losses = defaultdict(lambda: 0)
        for key, pr in pr_by_scales.items():
            gt = gt_by_scales[key]
            losses_s = self.forward_ss(pr, gt)
            for k, v in losses_s.items():
                losses[k] = losses[k] + v
        return losses

    def forward_ss(self, pr, gt):
        pr_obj, pr_cls, pr_off = pr
        gt_obj, gt_cls, gt_off = gt

        mask = (gt_obj).sum(dim=1) > 0
        loss_obj = sigmoid_focal_loss(pr_obj, gt_obj).mean()
        loss_cls = F.binary_cross_entropy_with_logits(pr_cls, gt_cls)
        loss_iou = iou_loss(pr_off, gt_off, mask).mean()
        loss_iou = loss_iou.nan_to_num()  # no boxes
        loss = loss_obj + loss_cls + loss_iou
        return {
            "total": loss,
            "objectness": loss_obj,
            "centerness": loss_cls,
            "offset": loss_iou,
        }


def groupby(x, keyfn):
    outputs = {}
    for x_i in x:
        key = keyfn(x_i)
        outputs[key] = x_i
    return outputs

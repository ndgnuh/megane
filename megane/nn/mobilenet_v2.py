from typing import List

import torch
from torch import nn


class InvertedResidualBlock(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, stride: int, expand: int):
        super().__init__()
        mid_channels = in_channels * expand
        common_kwargs = {"bias": False}
        dw_kwargs = {"padding": 1, "stride": stride, "groups": mid_channels}

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, 1, **common_kwargs),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU6(inplace=True),
            nn.Conv2d(mid_channels, mid_channels, 3, **dw_kwargs),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU6(inplace=True),
            nn.Conv2d(mid_channels, out_channels, 1, **common_kwargs),
            nn.BatchNorm2d(out_channels),
        )
        self.residual = (stride == 1) and (in_channels == out_channels)

    def forward(self, x):
        if self.residual:
            return x + self.conv(x)
        else:
            return self.conv(x)


def MobilenetV2Stage(
    in_channels: int,
    out_channels: int,
    num_layers: int,
    stride: int,
    expand: int,
):
    layers = []
    for i in range(num_layers):
        layer = InvertedResidualBlock(
            in_channels if (i == 0) else out_channels,
            out_channels,
            stride=stride if (i == 0) else 1,
            expand=expand,
        )
        layers.append(layer)
    return nn.Sequential(*layers)


class MobilenetV2(nn.Module):
    def __init__(
        self,
        channels: List[int] = [16, 16, 24, 32, 64, 96, 160, 320],
        num_layers: List[int] = [1, 2, 3, 4, 3, 3, 1],
        strides: List[int] = [1, 2, 2, 2, 1, 2, 1],
        expand: List[int] = [1, 6, 6, 6, 6, 6, 6],
        output_multiple_scale: bool = False,
    ):
        super().__init__()
        # Config validation
        msg = "Number of channels must be number of stages + 1"
        assert len(channels) == len(num_layers) + 1, msg
        msg = "Number of stages and number of strides does not match"
        assert len(strides) == len(num_layers), msg
        msg = "Number of stages and number of expand ratios does not match"
        assert len(expand) == len(num_layers), msg

        # Build layers
        stem = nn.Sequential(
            nn.Conv2d(3, channels[0], 3, 2, padding=1, bias=False),
            nn.BatchNorm2d(channels[0]),
            nn.ReLU6(True),
        )
        stages = [stem]
        num_stages = len(num_layers)
        for i in range(num_stages):
            stage = MobilenetV2Stage(
                channels[i],
                channels[i + 1],
                stride=strides[i],
                num_layers=num_layers[i],
                expand=expand[i],
            )
            stages.append(stage)

        # Register
        self.outputs = [False] + [stride == 2 for stride in strides]
        self.out_channels = [c for c, o in zip(channels, self.outputs) if o]
        self.stages = nn.ModuleList(stages)
        if output_multiple_scale:
            self.forward = self.forward_ms
        else:
            self.forward = self.forward_ss

    @property
    def out_strides(self):
        sz = 768
        img = torch.rand(1, 3, sz, sz)
        outputs = self(img)
        strides = []
        for o in outputs:
            strides.append(sz // o.shape[-1])
        return strides

    def forward_ss(self, x):
        for stage in self.stages:
            x = stage(x)
        return x

    def forward_ms(self, x):
        outputs = []
        for i, stage in enumerate(self.stages):
            x = stage(x)
            if self.outputs[i]:
                outputs.append(x)
        return outputs

from typing import List

import torch
from torch import nn

from .fcos import FCOSCriterion, FCOSHead
from .fpn import FeaturePyramid, ScaleFusion
from .mobilenet_v2 import MobilenetV2

assert FCOSCriterion


class SingleScaleFCOS(nn.Sequential):
    def __init__(self, num_classes: int):
        super().__init__()
        # Initialize
        backbone = MobilenetV2(output_multiple_scale=True)
        head_channels = (backbone.out_channels[-1] + backbone.out_channels[-2]) // 2
        fuse = ScaleFusion(backbone.out_channels, head_channels)
        head = FCOSHead(head_channels, num_classes)

        # Register
        self.backbone = backbone
        self.fuse = fuse
        self.head = head


class FCOS(nn.Module):
    def __init__(self, num_classes: int, strides: List[int]):
        super().__init__()
        # Mobilenet V2 backbone
        backbone = MobilenetV2(output_multiple_scale=True)
        bb_strides = backbone.out_strides
        bb_channels = backbone.out_channels

        # Calculate FPN specs from backbone
        ft_map_idx = [bb_strides.index(s) for s in strides if s in bb_strides]
        extra_strides = [s for s in strides if s not in bb_strides]
        num_extra_layers = len(extra_strides)
        extra_idx = [len(strides) + i for i in range(num_extra_layers)]

        # Feature pyramid network
        head_channels = (bb_channels[-1] + bb_channels[-2]) // 2
        fpn = FeaturePyramid(bb_channels, head_channels, num_extra_layers)
        num_heads = len(backbone.out_channels) + num_extra_layers

        # Prediction heads
        heads = nn.ModuleList()
        for _ in range(num_heads):
            layer = FCOSHead(head_channels, num_classes)
            heads.append(layer)

        # register
        self.backbone = backbone
        self.fpn = fpn
        self.heads = heads
        self.ft_map_idx = ft_map_idx + extra_idx
        self.set_post_processing(False)

    def forward(self, x):
        ft_maps = self.backbone(x)
        ft_maps = self.fpn(ft_maps)
        ft_maps = [ft_maps[i] for i in self.ft_map_idx]
        outputs = [self.heads[i](ft_map) for i, ft_map in enumerate(ft_maps)]
        return outputs

    def set_post_processing(self, on: bool):
        for m in self.modules():
            self.post_processing = on

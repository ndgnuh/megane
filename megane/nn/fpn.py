from typing import List

import torch
from torch import nn
from torch.nn import functional as F


class FeaturePyramid(nn.Module):
    def __init__(self, channels: List[int], head_size: int, num_extra_layers: int):
        super().__init__()
        num_scales = len(channels)

        # Projection
        in_branch = nn.ModuleList()
        for i in range(num_scales):
            in_channels = channels[i]
            layer = nn.Sequential(
                nn.Conv2d(in_channels, head_size, 1, bias=False),
                nn.BatchNorm2d(head_size),
                nn.ReLU(True),
            )
            in_branch.append(layer)

        # Extra layers to produce smaller feature maps
        extra_layers = nn.ModuleList()
        for i in range(num_extra_layers):
            layer = nn.Sequential(
                nn.Conv2d(head_size, head_size, 3, stride=2, padding=1),
                nn.BatchNorm2d(head_size),
                nn.ReLU(True),
            )
            extra_layers.append(layer)

        # Output projection branch
        out_branch = nn.ModuleList()
        num_out_layers = num_scales + num_extra_layers
        for i in range(num_out_layers):
            layer = nn.Sequential(
                nn.Conv2d(head_size, head_size, 3, padding=1, bias=False),
                nn.BatchNorm2d(head_size),
                nn.ReLU(True),
            )
            out_branch.append(layer)

        # Register
        self.num_scales = num_scales
        self.inter_kwargs = {"mode": "bilinear", "align_corners": True}
        self.extra_layers = extra_layers
        self.in_branch = in_branch
        self.out_branch = out_branch

    def forward(self, ft_maps):
        n = self.num_scales

        # Top down branch
        outputs = []
        for i in range(n):
            j = n - i - 1
            conv = self.in_branch[i]
            x = ft_maps[i]
            if i != 0:
                H, W = x.shape[-2:]
                prev = outputs[-1]
                prev = F.interpolate(prev, (H, W), **self.inter_kwargs)
                x = conv(x) + prev
            else:
                x = conv(x)
            outputs.append(x)

        # Extra top down outputs
        x = outputs[-1]
        for extra in self.extra_layers:
            x = extra(x)
            outputs.append(x)

        # Projection branch
        for i, layer in enumerate(self.out_branch):
            outputs[i] = layer(outputs[i])

        return outputs


class ScaleFusion(nn.Module):
    def __init__(self, channels: List[int], out_channels: int):
        super().__init__()
        # Check configurations
        num_scales = len(channels)
        assert out_channels % num_scales == 0
        head_channels = out_channels // num_scales

        # Build branches
        upsample_kwargs = dict(mode="bilinear", align_corners=True)
        in_branch = []
        out_branch = []
        for i in range(num_scales):
            scale_factor = 2 ** (num_scales - i - 1)
            in_layer = nn.Sequential(
                nn.Conv2d(channels[i], out_channels, 1, bias=False),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(True),
            )
            out_layer = nn.Sequential(
                nn.Conv2d(out_channels, head_channels, 3, padding=1, bias=False),
                nn.BatchNorm2d(head_channels),
                nn.ReLU(True),
                nn.Upsample(scale_factor=scale_factor, **upsample_kwargs),
            )
            in_branch.append(in_layer)
            out_branch.append(out_layer)

        # Register
        self.upsample = nn.Upsample(scale_factor=2, **upsample_kwargs)
        self.in_branch = nn.ModuleList(in_branch)
        self.out_branch = nn.ModuleList(out_branch)
        self.num_scales = num_scales

    def forward(self, ft_maps):
        n = self.num_scales

        # Project to same channels
        xs_1 = []
        for i in range(n):
            ft_map = ft_maps[i]
            ft_map = self.in_branch[i](ft_map)
            xs_1.append(ft_map)

        # Upscale and combine by sumation
        outputs = [xs_1[-1]]
        for i in range(1, n):
            tmp = self.upsample(outputs[i - 1]) + xs_1[n - i - 1]
            outputs.append(tmp)

        # Last projection + concat features
        outputs = [self.out_branch[i](outputs[i]) for i in range(n)]
        outputs = torch.cat(outputs, dim=1)
        return outputs


if __name__ == "__main__":
    import torch
    from mobilenet_v2 import MobilenetV2

    backbone = MobilenetV2(output_multiple_scale=True)
    img = torch.rand(1, 3, 300, 300)
    fpn = FeaturePyramid(backbone.out_channels, 96, 2)

    ft_maps = backbone(img)
    fpn(ft_maps)

import json
import warnings
from typing import Dict


def _read_toml(path: str):
    import toml

    with open(path) as f:
        config = toml.load(f)
    return config


def _read_yaml(path: str):
    import yaml

    with open(path) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


def _read_json(path: str):
    with open(path) as f:
        config = json.load(f)
    return config


class ConfigError(BaseException):
    @classmethod
    def wrong_type(Error, name, T, expectT):
        msg = f"Expected key `{name}` to have {expectT} type, got {T}"
        raise Error(msg)

    @classmethod
    def missing_key(Error, key):
        msg = f"Config key `{key}` not found"
        raise Error(msg)

    @classmethod
    def check(Error, cond, msg):
        if not cond:
            raise Error(msg)


def validate_config(
    config: Dict,
    train: bool = False,
    infer: bool = False,
    test: bool = False,
):
    # Setting tuple: [Key's name, key's type, required]
    settings = [
        ("num_classes", int, True),
        ("image_width", int, True),
        ("image_height", int, True),
        ("strides", (int, list), True),
        ("detection_threshold", float, True),
        ("nms_threshold", float, True),
        ("load_weights", str, infer),
        ("save_weights", str, train),
        ("onnx_path", str, infer),
        ("train_data", list, train),
        ("test_data", list, test),
        ("val_data", list, False),
        ("augment_prob", float, train),
        ("augment_features", list, train),
    ]

    # Model configurations validating
    for key_name, key_type, required in settings:
        if key_name not in config:
            if required:
                ConfigError.missing_key(key_name)
                break
            else:
                warnings.warn(
                    f"{key_name} is missing from config, but it is not required"
                )
                continue

        value = config[key_name]
        if not isinstance(value, key_type):
            ConfigError.wrong_type(key_name, type(value), key_type)

    # Value validations
    msg = "Train datasets can't not be empty when training"
    ConfigError.check(len(config["train_data"]) > 0 if train else True, msg)

    msg = "Test datasets can't not be empty when testing"
    ConfigError.check(len(config["test_data"]) > 0 if test else True, msg)

    msg = "Image size must be greater than 0"
    ConfigError.check(config["image_width"] > 0, msg)
    ConfigError.check(config["image_height"] > 0, msg)

    msg = "Number of classes must be greater than 0"
    ConfigError.check(config["num_classes"] > 0, msg)

    return config


def read_config(path: str):
    lpath = path.lower()
    if lpath.endswith(".json"):
        config = _read_json(path)
    elif lpath.endswith(".yaml") or lpath.endswith(".yml"):
        config = _read_yaml(path)
    elif lpath.endswith(".toml") or lpath.endswith(".tml"):
        config = _read_toml(path)
    else:
        raise ValueError(f"Unknown config format {path.split('.')[-1]}")
    validate_config(config, False, False, False)
    return config


def acceptable_config_formats():
    return ["json", "yaml", "toml", "yml"]


def example_config(name: str = "fcos"):
    # Model config
    config = {
        "num_classes": 10,
        "image_width": 300,
        "image_height": 300,
        "detection_threshold": 0.6,
        "nms_threshold": 0.4,
        "strides": [4, 8, 16, 32, 64, 128],
        # Weights and loading
        "load_weights": f"{name}-pretrained.pt",
        "save_weights": f"{name}-finetuned.pt",
        "onnx_path": f"{name}.onnx",
        # Train, test, etc.
        "train_data": ["train_data.rec"],
        "test_data": ["test_data.rec"],
        "val_data": ["optional.rec"],
        # Augmentations
        "augment_prob": 0.3333,
        "augment_features": [
            "color_transform",
            "weather_transform",
        ],
    }
    validate_config(config, True, True, True)
    return config

from copy import deepcopy
from typing import Callable, Dict, List, Optional

import cv2
import numpy as np
from dsrecords import IndexedRecordDataset, io
from lightning import LightningDataModule
from torch.utils.data import (ConcatDataset, DataLoader, Dataset,
                              default_collate)


class DetectionDataset(Dataset):
    def __init__(
        self,
        data_path: str,
        process_input: Optional[Callable] = None,
        encode_fn: Optional[Callable] = None,
        collate_fn: Optional[Callable] = default_collate,
        augment_fn: Optional[Callable] = None,
    ):
        # +-----------------+
        # | Try for schemas |
        # +-----------------+
        load_cv2 = io.load_cv2(flags=cv2.IMREAD_COLOR)
        try:
            has_class = True
            data = IndexedRecordDataset(data_path)
            data.deserializers = (load_cv2, io.load_np, io.load_np)
            [data[i] for i in range(10)]
        except Exception:
            has_class = False
            data = IndexedRecordDataset(data_path)
            data.deserializers = (load_cv2, io.load_np)
            [data[i] for i in range(10)]

        # +--------------------+
        # | Store informations |
        # +--------------------+
        self.has_class = has_class
        self.data = data
        self.process_input = process_input
        self.encode_fn = encode_fn
        self.collate_fn = collate_fn
        self.augment_fn = augment_fn

        # +-----------------------+
        # | Support for repeating |
        # +-----------------------+
        self.forced_len = None
        self.repeat_len = None

    def __len__(self):
        # +--------------------------------------------+
        # | In case of repeating, return forced length |
        # | else return true length                    |
        # +--------------------------------------------+
        return len(self.data) if self.forced_len is None else self.forced_len

    def repeat(self, n: Optional[int]):
        """Set `n` to some number to repeat the first `n` samples.
        Set `n` to `None` to disable repeat.
        """
        self.repeat_len = n
        self.forced_len = n * 1000

    def __getitem__(self, i: int):
        # +------------------------------+
        # | In case of repeating samples |
        # +------------------------------+
        if self.repeat_len is not None:
            i = i % self.repeat_len

        # +------------------------------+
        # | Attempt to retrieve the data |
        # +------------------------------+
        if self.has_class:
            image, boxes, classes = self.data[i]
        else:
            image, boxes = self.data[i]
            classes = np.zeros(boxes.shape[0], dtype=int)

        # +-----------------------------------------------------+
        # | Convert image to RGB, because we load them with BGR |
        # +-----------------------------------------------------+
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # +---------------------+
        # | Augmentation if any |
        # +---------------------+
        if self.augment_fn is not None:
            boxes = np.clip(boxes, 0, 1)
            result = self.augment_fn(image=image, bboxes=boxes, classes=classes)
            image = result["image"]
            boxes = result["bboxes"]
            classes = result["classes"]

        # +--------------------------+
        # | Preprocess input, if any |
        # +--------------------------+
        if self.process_input is not None:
            image = self.process_input(image)

        # +------------------------+
        # | Encode targets, if any |
        # +------------------------+
        if self.encode_fn is not None:
            return image, self.encode_fn((boxes, classes))
        else:
            return image, (boxes, classes)

    def get_dataloader(self, **kwargs):
        collate_fn = self.collate_fn if self.collate_fn else default_collate
        return DataLoader(self, **kwargs, collate_fn=collate_fn)


class DetectionDataModule(LightningDataModule):
    """Abstract away common dataloading stuffs.

    This module stays if it is useful.
    """

    def __init__(
        self,
        train_data: List[str] = [],
        val_data: List[str] = [],
        test_data: List[str] = [],
        # Dataset options, explicitly put here to avoid abstraction leak
        process_input: Optional[Callable] = None,
        encode_fn: Optional[Callable] = None,
        collate_fn: Optional[Callable] = default_collate,
        augment_fn: Optional[Callable] = None,
        dataloader_options: Dict = {},
    ):
        super().__init__()
        self.data_paths = dict(
            train=set(train_data),
            val=set(val_data),
            test=set(test_data),
        )
        self.dataset_options = {
            "process_input": process_input,
            "encode_fn": encode_fn,
            "collate_fn": collate_fn,
            "augment_fn": augment_fn,
        }
        self.dataloader_options = deepcopy(dataloader_options)

    def get_dataset(self, subset):
        options = deepcopy(self.dataset_options)
        if subset != "train":
            options["augment_fn"] = None
        paths = self.data_paths[subset]
        datasets = [DetectionDataset(path, **options) for path in paths]
        return ConcatDataset(datasets)

    def get_dataloader(self, subset, **override):
        data = self.get_dataset(subset)
        options = deepcopy(self.dataloader_options)
        options.update(override)
        return DataLoader(data, **options)

    def train_dataloader(self):
        return self.get_dataloader("train")

    def test_dataloader(self):
        return self.get_dataloader("test")

    def val_dataloader(self):
        return self.get_dataloader("val", shuffle=False)


if __name__ == "__main__":
    from functools import partial

    train_data = ["/home/hung/Data/face-detection/faces_30-10-2023.rec"]
    dm = DetectionDataModule(
        train_data=train_data, dataloader_options={"batch_size": 1, "shuffle": True}
    )
    print(next(iter(dm.train_dataloader())))
    print(dm)

    # image, boxes, classes = data[0]
    # print(image.shape, boxes.shape, classes.shape)

from typing import List

import albumentations as A

aug_features = {}


def add_feature(f):
    aug_features[f.__name__] = f
    return f


@add_feature
def color_transform(p: float):
    return A.OneOf(
        [
            A.ChannelShuffle(),
            A.ColorJitter(),
            A.CLAHE(),
            A.FancyPCA(),
            A.InvertImg(),
            A.Posterize(),
            A.Solarize(),
            A.Equalize(),
            A.HueSaturationValue(),
            A.RandomBrightnessContrast(),
            A.RandomToneCurve(),
            A.RGBShift(),
            A.ToGray(),
            A.ToSepia(),
            A.RingingOvershoot(),
        ],
        p=p,
    )


@add_feature
def weather_transform(p: float):
    return A.OneOf(
        [
            A.RandomRain(),
            A.Spatter(),
        ],
        p=p,
    )


def get_augmentation(p: float, features: List[str]):
    # Gather transformations based on presets
    # All the "features" should return a transformation
    all_transformations = []
    for feature in features:
        transformations = aug_features[feature](p)
        all_transformations.append(transformations)

    # No augmentation
    if len(all_transformations) == 0 or p <= 0:
        return None

    # Compose transformation
    bbox_params = A.BboxParams(
        format="albumentations",
        check_each_transform=False,
        label_fields=["classes"],
    )
    transform = A.Compose(all_transformations, p=1, bbox_params=bbox_params)
    return transform

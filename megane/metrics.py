from torch import nn
from torchmetrics.detection import MeanAveragePrecision


def to_torchmetrics_format(boxes, classes, scores=None):
    batch_size = len(boxes)
    outputs = []
    for b in range(batch_size):
        d = dict(boxes=boxes[b], labels=classes[b])
        if scores is not None:
            d["scores"] = scores[b]
        outputs.append(d)
    return outputs


class MeanAPCalculator(nn.Module):
    def __init__(self):
        super().__init__()
        self.meanap = MeanAveragePrecision()

    def forward(self, pr_boxes, pr_classes, pr_scores, gt_boxes, gt_classes):
        preds = to_torchmetrics_format(pr_boxes, pr_classes, pr_scores)
        targets = to_torchmetrics_format(gt_boxes, gt_classes)
        return self.meanap.update(preds, targets)


if __name__ == "__main__":
    from tqdm import tqdm
    from datasets import DetectionDataModule
    import torch
    from lightning import Fabric
    meanap = MeanAPCalculator()
    train_data = ["/home/hung/Data/face-detection/faces_30-10-2023.rec"]
    dm = DetectionDataModule(
        train_data=train_data, dataloader_options={"batch_size": 1, "shuffle": True}
    )
    data_loader = dm.train_dataloader()

    fabric = Fabric()
    meanap = fabric.setup(meanap)
    data_loader = fabric.setup_dataloaders(data_loader)
    for images, boxes, classes in tqdm(data_loader):
        scores = torch.rand(classes.shape) / 2 + 0.5
        m_ap = meanap.forward(boxes, classes, scores, boxes, classes)
    map_result = meanap.meanap.compute()
    print(map_result)
